const getElement = id => document.getElementById(id)

export const getValue = () => {
    let name = getElement('name').value,
        brand = getElement('brand').value,
        price = getElement('price').value,
        img = getElement('img').value,
        desc = getElement('desc').value
    return { name, brand, price, img, desc }
}

export const assignValue = (name = '', type = '', price = '', img = '', desc = '') => {
    getElement('name').value = name
    getElement('brand').value = type
    getElement('price').value = price
    getElement('img').value = img
    getElement('desc').value = desc
}