function Validation() {
    const getElement = id => document.getElementById(id)
    this.kiemTraRong = (value, id) => {
        if (value.trim() === '') {
            getElement(id).innerHTML = 'Không được bỏ trống'
            getElement(id).style.display = 'block'
            return false
        } else {
            getElement(id).innerHTML = ''
            getElement(id).style.display = 'none'
            return true
        }
    }
    this.kiemTraEmail = (value, id) => {
        let email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\ [[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (email.test(value)) {
            getElement(id).innerHTML = ''
            getElement(id).style.display = 'none'
            return true
        } else {
            getElement(id).innerHTML = 'Email không hợp lệ'
            getElement(id).style.display = 'block'
            return false
        }
    }
    this.kiemTraSo = (value, id) => {
        let so = /^[0-9]+$/
        if (so.test(value)) {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true
        } else {
            getElement(id).innerHTML = 'Phải nhập số';
            getElement(id).style.display = 'block';
            return false;
        }
    }
    this.kiemTraChuVaSo = (value, id) =>{
        let so = /^[a-zA-Z0-9\s]*$/
        if (so.test(value)) {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true
        } else {
            getElement(id).innerHTML = 'Không được nhập kí tự đặc biệt';
            getElement(id).style.display = 'block';
            return false;
        }
    }
    this.kiemTraChu = (value, id) => {
        let chu = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/
        if (chu.test(value)) {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true
        } else {
            getElement(id).innerHTML = 'Phải nhập chữ';
            getElement(id).style.display = 'block';
            return false;
        }
    }
    this.kiemTraDoDai = (value, id, min, max) => {
        if (value.trim().length < min || value.trim().length > max) {
            getElement(id).innerHTML = `Độ dài phải từ ${min} đến ${max} kí tự`;
            getElement(id).style.display = 'block';
            return false
        } else {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true;
        }

    }
    this.kiemTraDuongDan = (value, id) => {
        let link = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/
        if (link.test(value)) {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true
        } else {
            getElement(id).innerHTML = 'Phải nhập đường dẫn ảnh';
            getElement(id).style.display = 'block';
            return false;
        }
    }
    this.kiemTraGiaTri = (value, id, min, max) => {
        if (Number(value) < min || Number(value) > max) {
            getElement(id).innerHTML = `Giá trị phải từ ${min.toLocaleString()} đến ${max.toLocaleString()} (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧`;
            getElement(id).style.display = 'block';
            return false
        } else {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true;
        }
    }
    this.kiemTrTonTai = (value,list,id) =>{
        let index = list.findIndex(item => item.name.toLowerCase() === value.toLowerCase().trim())
        console.log(index)
        if (index === -1) {
            getElement(id).innerHTML = '';
            getElement(id).style.display = 'none';
            return true
        } else {
            getElement(id).innerHTML = 'Sản phẩm đã tồn tại';
            getElement(id).style.display = 'block';
            return false;
        }
    }
}

export default Validation
